#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <math.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "lib/timer.h"
#include "lib/commandlinereader.h"
#include "lib/hashtable.h"

#define MAXARGS 6
#define BUFFERSIZE 512
#define HASH_MOD 500

float MAXCHILDREN = INFINITY;
unsigned long ACTIVECHILDREN = 0;

char buffer[BUFFERSIZE];
char **argVector;
FILE *fin;
FILE *finished;
htable_t *clients;
htable_t *childs;

struct sigaction act;

// Structure to store clients
typedef struct{
	pid_t PID;
	FILE *pipe;
} client_t;

// Structure to store childs
typedef struct{
	pid_t PID;
	TIMER_T start;
} child_t;

// Hash function for clients
int hashClient(void *elem){
	return (((client_t *) elem)->PID) % HASH_MOD;
}

// Hash function for childs
int hashChild(void *elem){
	return (((child_t *) elem)->PID) % HASH_MOD;
}

// Compare 2 clients
int compClient(void *elem1, void *elem2){
	return (((client_t *) elem1)->PID - ((client_t *) elem2)->PID);
}

// Compare 2 child
int compChild(void *elem1, void *elem2){
	return (((child_t *) elem1)->PID - ((child_t *) elem2)->PID);
}

// Remove client
void freeClient(void *elem){
	if(fclose(((client_t *) elem)->pipe) == EOF && errno != EBADF){
		perror("fclose");
		exit(1);
	}
	free(elem);
}

// Remove child
void freeChild(void *elem){
	free(elem);
}

// Signal handling
void handle_signal(int sig){
	int status;
	pid_t PID;

	TIMER_T stop;
	TIMER_READ(stop);

	if((PID = wait(&status)) == -1){
		perror(NULL);
	}

	child_t search;
	search.PID = PID;
	child_t *child = searchHTable(childs, &search, compChild);

	if((fprintf(finished, "%d %d %d\n", PID, status, (int) TIMER_DIFF_SECONDS(child->start, stop))) < 0){
		fprintf(stderr, "fgets(): An error occurred\n");
	}

	ACTIVECHILDREN--;
}

void run(char **args, int argsRead){
	// Choose between SeqSolver and ParSolver
	if(argsRead == 2){
		if((args[0] = strdup("CircuitRouter-SeqSolver")) == NULL){
			perror("strdup");
			exit(1);
		}

		execv("./CircuitRouter-SeqSolver/CircuitRouter-SeqSolver", args);
	}else{
		if((args[0] = strdup("CircuitRouter-ParSolver")) == NULL){
			perror("strdup");
			exit(1);
		}

		execv("./CircuitRouter-ParSolver/CircuitRouter-ParSolver", args);
	}

	perror("execv");
	exit(1);
}

// Handle stdin
void handle_stdin(int *quit){
	int argsRead = readLineArguments(stdin, argVector, MAXARGS + 1, buffer, BUFFERSIZE);

	if(argsRead == 0 || argsRead == -1){
		return;
	}

	if(!strcmp("exit", argVector[0])){
		if(argsRead > 1){
			printf("Incorrect use of command exit!\n");
			printf("Usage: exit\n");
		}else{
			*quit = 1;
			return;
		}
	}else if(!strcmp("run", argVector[0])){
		if(argsRead != 2 && argsRead != 4){
			printf("Incorrect usage of command run!\n");
			printf("Minimal usage: run <inputfile>\n");
			printf("Extended usage: run <inputfile> -t <numthreads>\n");
			return;
		}else{
			while(ACTIVECHILDREN == MAXCHILDREN){
				// No need to handle EINTR because it will always occur after pause returns
				pause();
			}

			pid_t PID = fork();

			if(PID == 0){
				// Discard Seq/ParSolver output
				int fd;
				if((fd = open("/dev/null", O_RDWR)) == -1){
					perror("open");
					exit(1);
				}

				// Child process will comunicate with client using file descriptor 3
				if(dup2(fd, 3) == -1){
					perror("dup2");
					exit(1);
				}

				// Prepare arguments for run command
				char *args[argsRead+1];
				args[argsRead] = NULL;
				int i;

				for(i = 1; i < argsRead; i++){
					if((args[i] = strdup(argVector[i])) == NULL){
						perror("strdup");
						exit(1);
					}
				}

				run(args, argsRead);
			}else if(PID == -1){
				perror("fork");
				exit(1);
			}

			child_t *child = malloc(sizeof(child_t));
			child->PID = PID;
			TIMER_READ(child->start);

			addElemHTable(childs, child);

			ACTIVECHILDREN++;
		}
	// Unknown command
	}else{
		printf("Unknown command!\n");
		printf("Supported: run, exit\n");
	}
}

// Handle input pipe
void handle_pipe(){
	int argsRead = readLineArguments(fin, argVector, MAXARGS + 1, buffer, BUFFERSIZE);

	if(argsRead == 0 || argsRead == -1){
		return;
	}

	// Add client
	if(!strcmp("new", argVector[0])){
		client_t *client = malloc(sizeof(client_t));
		client->PID = atoi(argVector[1]);
		client->pipe = fopen(argVector[2], "a+");

		if(client->pipe == NULL){
			perror("fopen");
			exit(1);
		}

		addElemHTable(clients, client);

		// Confirm connection
		if(fprintf(client->pipe, "Connected\n") < 0){
			fprintf(stderr, "fprintf: An error occurred\n");
			exit(1);
		}

		if(fflush(client->pipe) == EOF){
			perror("fflush");
			exit(1);
		}

	// Remove client
	}else if(!strcmp("rem", argVector[0])){
		client_t search;
		search.PID = atoi(argVector[1]);
		client_t *client = (client_t *) popElemHTable(clients, &search, compClient);

		if(client == NULL){
			printf("Intruder client found!\n");
			return;
		}

		if(fclose(client->pipe) == EOF){
			perror("fclose");
			exit(1);
		}

		free(client);
	// Run command
	}else if(!strcmp("run", argVector[1])){
		// Search for the client
		client_t search;
		search.PID = atoi(argVector[0]);
		client_t *client = searchHTable(clients, &search, compClient);

		if(client == NULL){
			printf("Intruder client found!\n");
			return;
		}

		// If run command is incorrect
		if(argsRead != MAXARGS-1 && argsRead != 3){
			FILE *fout = client->pipe;
			if(fprintf(fout, "Usage (-t flag is optional): run <inputfile> -t <numthreads>\n") < 0){
				fprintf(stderr, "fgets(): An error ocurred\n");
			}

			if(fflush(fout) == EOF){
				perror("fflush");
				exit(1);
			}

			return;
		}else{
			while(ACTIVECHILDREN == MAXCHILDREN){
				// No need to handle EINTR because it will always occur after pause returns
				pause();
			}

			pid_t PID = fork();

			if(PID == 0){
				// Child process will comunicate with client using file descriptor 3
				if(dup2(fileno(client->pipe), 3) == -1){
					perror("dup2");
					exit(1);
				}

				// Prepare arguments for run command
				char *args[argsRead];

				// Because pipe reads 1 more argument than stdin
				argsRead--;

				args[argsRead] = NULL;

				int i;
				for(i = 1; i < argsRead; i++){
					if((args[i] = strdup(argVector[i+1])) == NULL){
						perror("strdup");
						exit(1);
					}
				}

				run(args, argsRead);
			}else if(PID == -1){
				perror("fork");
				exit(1);
			}

			child_t *child = malloc(sizeof(child_t));
			child->PID = PID;
			TIMER_READ(child->start);

			addElemHTable(childs, child);

			ACTIVECHILDREN++;
		}
	}else{
		// Search for the client
		client_t search;
		search.PID = atoi(argVector[0]);
		client_t *client = searchHTable(clients, &search, compClient);

		if(client == NULL){
			printf("Intruder client found!\n");
			return;
		}

		// Send error message
		FILE *fout = client->pipe;
		if(fprintf(fout, "Command not supported\n") < 0){
			fprintf(stderr, "fgets(): An error occurred\n");
		}

		if(fflush(fout) == EOF){
			perror("fflush");
			exit(1);
		}
	}
}

int main(int argc, char **argv){
	int quit = 0;

	// Initialize global structures
	argVector = malloc(sizeof(char *) * (MAXARGS + 1));
	createHTable(&clients, hashClient, HASH_MOD);
	createHTable(&childs, hashChild, HASH_MOD);

	sigemptyset(&act.sa_mask);
	act.sa_flags= SA_NOCLDSTOP|SA_RESTART;
	act.sa_handler=&handle_signal;

	// Install action to handle signal
	if(sigaction(SIGCHLD, &act, NULL) < 0){
		perror("sigaction");
		exit(1);
	}

	// Define the maximum number of child processes
	if(argc == 2){
		MAXCHILDREN = atol(argv[1]);

		if(MAXCHILDREN <= 0){
			printf("The maximum number of child processes must be a positive number\n");
			exit(1);
		}
	}else if(argc > 2){
		printf("Unknown parameters found\n");
		exit(1);
	}

	// Get name of the pipe
	char inpipe[] = "/tmp/ADV.pipe";
	// FIXME
	/*
	char inpipe[strlen(argv[0])+6];
	strcpy(inpipe, argv[0]);
	strcat(inpipe, ".pipe");*/

	// Create named pipe
	if((unlink(inpipe) == -1) && (errno != ENOENT)){
		perror("unlink");
		exit(1);
	}

	if(mkfifo(inpipe, 0666) == -1){
		perror("mkfifo");
		exit(1);
	}

	int fdin;

	if((fdin = open(inpipe, O_RDWR | O_NONBLOCK)) == -1){
		perror("open");
		exit(1);
	}

	fin = fdopen(fdin, "r");
	if(fin == NULL){
		perror("fdopen");
		exit(1);
	}

	// Temporary file to store PID and return values of child processes
	if((finished = tmpfile()) == NULL){
		perror("tmpfile");
		exit(1);
	}

	fd_set listen;

	do{
		// Set file descriptor mask
		FD_ZERO(&listen);
		FD_SET(fileno(stdin), &listen);
		FD_SET(fileno(fin), &listen);

		if(select(fileno(fin)+1, &listen, NULL, NULL, NULL) == -1){
			if(errno == EINTR){
				continue;
			}else{
				perror("select");
				exit(1);
			}
		}

		// Input chooser
		if(FD_ISSET(fileno(fin), &listen)){
			handle_pipe();
		}else{
			handle_stdin(&quit);
		}

	}while(!quit);

	// Wait for running child processes
	while(ACTIVECHILDREN){
		pause();
	}

	// Place the file cursor at the beginning of the file
	if(fseek(finished, 0, SEEK_SET) != 0){
		perror(NULL);
		exit(1);
	}

	pid_t PID;
	int status;
	int time;

	// Print the values in the temporary file
	while(fscanf(finished, "%d %d %d", &PID, &status, &time) != EOF){
		printf("CHILD EXITED (PID=%d; return %s; %d s)\n", PID,
				WIFEXITED(status) && status == 0 ? "OK" : "NOK", time);
	}

	// Close files and remove named pipe
	if(fclose(finished) == EOF){
		perror("fclose");
		exit(1);
	}

	if(fclose(fin) == EOF){
		perror("fclose");
		exit(1);
	}

	if(unlink(inpipe) == -1){
		perror("unlink");
		exit(1);
	}

	// Remove hashtables
	freeHTable(&clients, freeClient);
	freeHTable(&childs, freeChild);

	printf("END.\n");
	free(argVector);

	return 0;
}
