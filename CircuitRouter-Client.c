#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "lib/commandlinereader.h"

#define MAXARGS 3
#define BUFFERSIZE 512

#define NEW_PIPE_CMD "new"
#define REM_PIPE_CMD "rem"

int main(int argc, char **argv){
	char buffer[BUFFERSIZE];

	pid_t PID = getpid();

	if(argc != 2){
		printf("Usage: ./CircuitRouter-Client <pipe-name>\n");
		exit(1);
	}

	// Connect to AdvShell pipe
	FILE *fout = fopen(argv[1], "a+");
	if(fout == NULL){
		perror("fopen");
		exit(1);
	}

	// Create pipe to receive messages
	char inname[] = "/tmp/ClientXXXXXX";
	int fdin;

	// For safe name creation
	if((fdin = mkstemp(inname)) == -1){
		perror("mkstemp");
		exit(1);
	}

	close(fdin);
	
	if((unlink(inname) == -1) && (errno != ENOENT)){
		perror("unlink");
		exit(1);
	}

	if(mkfifo(inname, 0666) == -1){
		perror("mkfifo");
		exit(1);
	}
	
	//Send PID and named pipe
	if(fprintf(fout, "%s %d %s\n", NEW_PIPE_CMD, PID, inname) < 0){
		printf("fgets(): An error ocurred\n");
	}

	if(fflush(fout) == EOF){
		perror("fflush");
		exit(1);
	}

	// Open named pipe and wait for confirmation
	if((fdin = open(inname, O_RDONLY)) == -1){
		perror("open");
		exit(1);
	}

	FILE *fin = fdopen(fdin, "r");

	if(fin == NULL){
		perror("fopen");
		exit(1);
	}

	if(fgets(buffer, BUFFERSIZE, fin) == NULL){
		fprintf(stderr, "fgets() error!\n");
	}

	if(strcmp("Connected\n", buffer)){
		printf("Given pipe is incompatible\n");
		exit(1);
	}

	int quit = 0;

	do{
		// Command parsing

		while(fgets(buffer, BUFFERSIZE, stdin) == NULL);

		// If command is exit tell shell to remove client and exit
		if(!strcmp("exit\n", buffer)){
			if(fprintf(fout, "%s %d", REM_PIPE_CMD, PID) < 0){
				printf("fgets(): An error ocurred\n");
			}

			if(fflush(fout) == EOF){
				perror("fflush");
				exit(1);
			}

			quit = 1;
		}else{
			// Send command to shell
			if(fprintf(fout, "%d %s", PID, buffer) < 0){
				fprintf(stderr, "fprintf: An error occurred\n");
			}
			
			if(fflush(fout) == EOF){
				perror("fflush");
				exit(1);
			}

			// Get response from shell
			if(fgets(buffer, BUFFERSIZE, fin) == NULL){
				fprintf(stderr, "fgets() error!\n");
			}

			printf("%s", buffer);
		}
	}while(!quit);

	// Close files and remove pipe
	if(fclose(fout) == EOF){
		perror("close");
		exit(1);
	}

	if(fclose(fin) == EOF){
		perror("close");
		exit(1);
	}	

	if(unlink(inname) == -1){
		perror("unlink");
		exit(1);
	}

	return 0;
}
