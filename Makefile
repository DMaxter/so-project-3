SOURCES-SEQ = CircuitRouter-SeqSolver/router.c CircuitRouter-SeqSolver/maze.c CircuitRouter-SeqSolver/grid.c CircuitRouter-SeqSolver/coordinate.c CircuitRouter-SeqSolver/CircuitRouter-SeqSolver.c
SOURCES-PAR = CircuitRouter-ParSolver/router.c CircuitRouter-ParSolver/maze.c CircuitRouter-ParSolver/grid.c CircuitRouter-ParSolver/coordinate.c CircuitRouter-ParSolver/CircuitRouter-ParSolver.c
SOURCES-ADV = CircuitRouter-AdvShell.c
SOURCES-CLT = CircuitRouter-Client.c
LIBS = lib/vector.c lib/queue.c lib/list.c lib/pair.c lib/commandlinereader.c lib/hashtable.c

OBJS-SEQ = $(SOURCES-SEQ:%.c=%.o)
OBJS-PAR = $(SOURCES-PAR:%.c=%.o)
OBJS-ADV = $(SOURCES-ADV:%.c=%.o)
OBJS-CLT = $(SOURCES-CLT:%.c=%.o)
OBJS-LIB = $(LIBS:%.c=%.o)
OBJS = $(OBJS-SEQ) $(OBJS-PAR) $(OBJS-ADV) $(OBJS-CLT) $(OBJS-LIB)

CC   = gcc
CFLAGS =-g -Wall -std=gnu99 -pthread -I ./
LDFLAGS=-lm

TARGET-SEQ = CircuitRouter-SeqSolver/CircuitRouter-SeqSolver
TARGET-PAR = CircuitRouter-ParSolver/CircuitRouter-ParSolver
TARGET-ADV = CircuitRouter-AdvShell
TARGET-CLT = CircuitRouter-Client

all: $(TARGET-SEQ) $(TARGET-PAR) $(TARGET-ADV) $(TARGET-CLT)

$(TARGET-SEQ): $(OBJS-SEQ) $(OBJS-LIB)
	$(CC) $(CFLAGS) $^ -o $(TARGET-SEQ) $(LDFLAGS)

$(TARGET-PAR): $(OBJS-PAR) $(OBJS-LIB)
	$(CC) $(CFLAGS) $^ -o $(TARGET-PAR) $(LDFLAGS)

$(TARGET-ADV): $(OBJS-ADV) $(OBJS-LIB)
	$(CC) $(CFLAGS) $^ -o $(TARGET-ADV) $(LDFLAGS)

$(TARGET-CLT): $(OBJS-CLT) $(OBJS-LIB)
	$(CC) $(CFLAGS) $^ -o $(TARGET-CLT) $(LDFLAGS)


# CircuitRouter-AdvShell
CircuitRouter-AdvShell.o: CircuitRouter-AdvShell.c lib/commandlinereader.h lib/hashtable.h

# CircuitRouter-Client
CircuitRouter-Client.o: CircuitRouter-Client.c lib/commandlinereader.h

# CircuitRouter-SeqSolver
CircuitRouter-SeqSolver/CircuitRouter-SeqSolver.o: CircuitRouter-SeqSolver/CircuitRouter-SeqSolver.c CircuitRouter-SeqSolver/maze.h CircuitRouter-SeqSolver/router.h lib/list.h lib/timer.h lib/types.h
CircuitRouter-SeqSolver/router.o: CircuitRouter-SeqSolver/router.c CircuitRouter-SeqSolver/router.h CircuitRouter-SeqSolver/coordinate.h CircuitRouter-SeqSolver/grid.h lib/queue.h lib/vector.h
CircuitRouter-SeqSolver/maze.o: CircuitRouter-SeqSolver/maze.c CircuitRouter-SeqSolver/maze.h CircuitRouter-SeqSolver/coordinate.h CircuitRouter-SeqSolver/grid.h lib/list.h lib/queue.h lib/pair.h lib/types.h lib/vector.h
CircuitRouter-SeqSolver/grid.o: CircuitRouter-SeqSolver/grid.c CircuitRouter-SeqSolver/grid.h CircuitRouter-SeqSolver/coordinate.h lib/types.h lib/vector.h
CircuitRouter-SeqSolver/coordinate.o: CircuitRouter-SeqSolver/coordinate.c CircuitRouter-SeqSolver/coordinate.h lib/pair.h lib/types.h

# Libs
lib/vector.o: lib/vector.c lib/vector.h lib/types.h lib/utility.h
lib/queue.o: lib/queue.c lib/queue.h lib/types.h
lib/list.o: lib/list.c lib/list.h lib/types.h
lib/pair.o: lib/pair.c lib/pair.h
lib/commandlinereader.o: lib/commandlinereader.c lib/commandlinereader.h
lib/hashtable.o: lib/hashtable.c lib/hashtable.h

# CircuitRouter-ParSolver
CircuitRouter-ParSolver/CircuitRouter-ParSolver.o: CircuitRouter-ParSolver/CircuitRouter-ParSolver.c CircuitRouter-ParSolver/maze.h CircuitRouter-ParSolver/router.h lib/list.h lib/timer.h lib/types.h
CircuitRouter-ParSolver/router.o: CircuitRouter-ParSolver/router.c CircuitRouter-ParSolver/router.h CircuitRouter-ParSolver/coordinate.h CircuitRouter-ParSolver/grid.h lib/queue.h lib/vector.h
CircuitRouter-ParSolver/maze.o: CircuitRouter-ParSolver/maze.c CircuitRouter-ParSolver/maze.h CircuitRouter-ParSolver/coordinate.h CircuitRouter-ParSolver/grid.h lib/list.h lib/queue.h lib/pair.h lib/types.h lib/vector.h
CircuitRouter-ParSolver/grid.o: CircuitRouter-ParSolver/grid.c CircuitRouter-ParSolver/grid.h CircuitRouter-ParSolver/coordinate.h lib/types.h lib/vector.h
CircuitRouter-ParSolver/coordinate.o: CircuitRouter-ParSolver/coordinate.c CircuitRouter-ParSolver/coordinate.h lib/pair.h lib/types.h

$(OBJS):
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	@echo Cleaning...
	rm -f $(OBJS) $(TARGET-PAR) $(TARGET-SEQ) $(TARGET-ADV) $(TARGET-CLT)

objclean:
	@echo Cleaning...
	rm -f $(OBJS)
