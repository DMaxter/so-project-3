#ifndef HASHTABLE_H
#define HASHTABLE_H

typedef struct slist slist_t;

typedef struct htable htable_t;

void createHTable(htable_t **table, int (*hash)(void *), unsigned int N);
void addElemHTable(htable_t *table, void *elem);
void* searchHTable(htable_t *table, void *elem, int (*compCont)(void *, void *));
void* popElemHTable(htable_t *table, void *elem, int (*compCont)(void *, void *));
void removeElemHTable(htable_t *table, void *elem, int (*compCont)(void *, void *), void (*freeCont)(void *));
void freeHTable(htable_t **table, void (*freeCont)(void *));
#endif
