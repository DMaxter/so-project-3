#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

/* Simple lists implementation
 */

struct slist{
	void *content;
	struct slist *next;
};

void createSList(slist_t **head){
	*head = NULL;
}

void addElemSList(slist_t **head, void *elem){
	// Allocate new node
	slist_t *new = malloc(sizeof(slist_t));
	// Initialize contents
	new->next = *head;
	new->content = elem;
	// Change head
	*head = new;
}

void* searchSList(slist_t *head, void *elem, int (*compCont)(void *, void *)){
	slist_t *aux = head;

	while(aux != NULL){
		if(compCont(elem, aux->content) == 0){
			return aux->content;
		}

		aux = aux->next;
	}

	return NULL;
}

void* popElemSList(slist_t **head, void *elem, int (*compCont)(void *, void *)){
	slist_t *aux1 = *head;
	slist_t *aux2;
	void *ret;

	while(aux1 != NULL){
		if(compCont(elem, aux1->content) == 0){
			ret = aux1->content;

			// If it's head, change head
			if(aux1 == *head){
				*head = aux1->next;
			}else{
				aux2->next = aux1->next;
			}

			free(aux1);

			return ret;
		}

		aux2 = aux1;
		aux1 = aux1->next;
	}

	return NULL;
}

void removeElemSList(slist_t **head, void *elem, int (*compCont)(void *, void *), void (*freeCont)(void *)){
	void *ret;
	ret = popElemSList(head, elem, compCont);

	if(ret != NULL){
		freeCont(ret);
	}
}

void freeSList(slist_t **head, void (*freeCont)(void *)){
	slist_t *aux;

	while(*head != NULL){
		freeCont((*head)->content);
		aux = (*head)->next;
		free(*head);
		*head = aux;
	}
}

/* Hashtable implementation
 */
struct htable{
	unsigned int N;
	int (*hash)(void *);
	slist_t **table;
};

void createHTable(htable_t **table,int (*hash)(void *), unsigned int N){
	// Allocate hashtable
	*table = malloc(sizeof(htable_t));
	(*table)->hash = hash;
	(*table)->N = N;
	// Allocate table
	(*table)->table = malloc(sizeof(slist_t *) * N);

	// Create table entries
	unsigned int i;
	for(i = 0; i < N; i++){
		createSList(&((*table)->table)[i]);
	}
}

void addElemHTable(htable_t *table, void *elem){
	// Add element to corresponding list
	addElemSList(&(table->table[table->hash(elem)]), elem);
}

void* searchHTable(htable_t *table, void *elem, int (*compCont)(void *, void *)){
	// Return the element
	return searchSList(table->table[table->hash(elem)], elem, compCont);
}

void* popElemHTable(htable_t *table, void *elem, int (*compCont)(void *, void *)){
	return popElemSList(&(table->table[table->hash(elem)]), elem, compCont);
}

void removeElemHTable(htable_t *table, void *elem, int (*compCont)(void *, void *), void (*freeCont)(void *)){
	// Remove element from corresponding list
	removeElemSList(&(table->table[table->hash(elem)]), elem, compCont, freeCont);
}

void freeHTable(htable_t **table, void (*freeCont)(void *)){
	// Free table entries
	unsigned int i;
	for(i = 0; i < (*table)->N; i++){
		freeSList(&((*table)->table[i]), freeCont);
	}

	// Free table
	free((*table)->table);
	// Free structure
	free(*table);

	*table = NULL;
}
